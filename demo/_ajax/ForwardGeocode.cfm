
<cfparam name="Request.Attributes.AccessKey" default="#Application.Settings.AccessKey#">

<cfset PositionStack = CreateObject('Component', 'lib.PositionStack.PositionStack')>
<cfset PositionStack.SetAccessKey(Request.Attributes.AccessKey)>

<cfset stGeocode = PositionStack.Geocode(
	Address = Request.Attributes.Query,
	Country = Request.Attributes.Country,
	Region = Request.Attributes.Region,
	Language = Request.Attributes.Language,
	Country_Module = Request.Attributes.Country_Module,
	Sun_Module = Request.Attributes.Sun_Module,
	Timezone_Module = Request.Attributes.Timezone_Module,
	Bbox_Module = Request.Attributes.Bbox_Module,
	Limit = Request.Attributes.Limit,
	Fields = Request.Attributes.Fields,
	Output = Request.Attributes.Output,
	Callback = Request.Attributes.Callback
)>

<h2>Code</h2>

<h2>Response</h2>

<div class="row pt-2" style="margin: 0 2px;">
	<cfdump var="#stGeocode#">
</div>
