
<cfoutput>
	<div class="row">
		<h1>Forward Geocode</h1>

		<p>This takes an address and converts it into a latitude longitude.</p>
	</div>

	<div class="row">
		<h2>Parameters</h2>
	</div>

	<div class="row">
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Parameter</th>
					<th>Type</th>
					<th>Required</th>
					<th>Default</th>
					<th>Value</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Address</td>
					<td>string</td>
					<td>required</td>
					<td></td>
					<td><input type="text" id="Query" name="Query" value="" placeholder="Address" class="form-control"></td>
				</tr>
				<tr>
					<td>Country</td>
					<td>string</td>
					<td>optional</td>
					<td></td>
					<td><input type="text" id="Country" name="Country" value="" placeholder="Country Codes such as US, AU" class="form-control"></td>
				</tr>
				<tr>
					<td>Region</td>
					<td>string</td>
					<td>optional</td>
					<td></td>
					<td><input type="text" id="Region" name="Region" value="" placeholder="Region such as Berlin" class="form-control"></td>
				</tr>
				<tr>
					<td>Language</td>
					<td>string</td>
					<td>optional</td>
					<td>en</td>
					<td><input type="text" id="Language" name="Language" value="" placeholder="2 or 3 digit language code such as EN or ES" class="form-control"></td>
				</tr>
				<tr>
					<td>Country_Module</td>
					<td>boolean</td>
					<td>optional</td>
					<td>0</td>
					<td><input type="checkbox" id="Country_Module" name="Country_Module" value="1" class="form-control"></td>
				</tr>
				<tr>
					<td>Sun_Module</td>
					<td>boolean</td>
					<td>optional</td>
					<td>0</td>
					<td><input type="checkbox" id="Sun_Module" name="Sun_Module" value="1" class="form-control"></td>
				</tr>
				<tr>
					<td>Timezone_Module</td>
					<td>boolean</td>
					<td>optional</td>
					<td>0</td>
					<td><input type="checkbox" id="Timezone_Module" name="Timezone_Module" value="1" class="form-control"></td>
				</tr>
				<tr>
					<td>Bbox_Module</td>
					<td>boolean</td>
					<td>optional</td>
					<td>0</td>
					<td><input type="checkbox" id="Bbox_Module" name="Bbox_Module" value="1" class="form-control"></td>
				</tr>
				<tr>
					<td>Limit</td>
					<td>integer</td>
					<td>optional</td>
					<td>10</td>
					<td><input type="text" id="Limit" name="Limit" value="10" class="form-control"></td>
				</tr>
				<tr>
					<td>Fields</td>
					<td>string</td>
					<td>optional</td>
					<td></td>
					<td><input type="text" id="Fields" name="Fields" value="" placeholder="" class="form-control"></td>
				</tr>
				<tr>
					<td>Output</td>
					<td>string</td>
					<td>optional</td>
					<td></td>
					<td><input type="text" id="Output" name="Output" value="" placeholder="" class="form-control"></td>
				</tr>
				<tr>
					<td>Callback</td>
					<td>string</td>
					<td>optional</td>
					<td></td>
					<td><input type="text" id="Callback" name="Callback" value="" placeholder="" class="form-control"></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="row">
		<button id="btnForwardGeocode" class="btn btn-primary" type="submit">Try <i class="fas fa-arrow-right"></i></button>
	</div>

	<div class="row mt-4">
		<div id="divResults"></div>
	</div>
</cfoutput>

