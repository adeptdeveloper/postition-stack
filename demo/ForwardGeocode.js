
$(document).ready(function()
{
	$('#btnForwardGeocode').click(function()
	{
		var Query = $('#Query').val();
		var Country = $('#Country').val();
		var Region = $('#Region').val();
		var Language = $('#Language').val();
		var Country_Module = $('#Country_Module').prop('checked');
		var Sun_Module = $('#Sun_Module').prop('checked');
		var Timezone_Module = $('#Timezone_Module').prop('checked');
		var Bbox_Module = $('#Bbox_Module').prop('checked');
		var Limit = $('#Limit').val();
		var Fields = $('#Fields').val();
		var Output = $('#Output').val();
		var Callback = $('#Callback').val();

		$('#divResults').load(
			'_ajax/ForwardGeocode.cfm',
			{
				'Query': Query,
				'Country': Country,
				'Region': Region,
				'Language': Language,
				'Country_Module': Country_Module,
				'Sun_Module': Sun_Module,
				'Timezone_Module': Timezone_Module,
				'Bbox_Module': Bbox_Module,
				'Limit': Limit,
				'Fields': Fields,
				'Output': Output,
				'Callback': Callback
			},
			function()
			{

			}
		);
	});
});
