
				</main>
			</div>

			<nav class="navbar fixed-bottom navbar-light bg-light">
				<span class="navbar-text small">Copyright 2020<cfif Year(Now()) GT 2020>-#Year(Now())#</cfif> Adept Developer, LLC.  All rights reserved.</span>
				<a href="https://www.adeptdeveloper.com/" class="small">www.adeptdeveloper.com</a>
			</nav>
		</div>

		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

		<script src="https://kit.fontawesome.com/3656532ad7.js" crossorigin="anonymous"></script>

		<cfset IncludeJSFile = ReplaceNoCase(CGI.SCRIPT_NAME, '.cfm', '.js')>

		<cfif FileExists(ExpandPath(IncludeJSFile))>
			<cfoutput>
				<script src="#Application.Settings.HttpRoot##IncludeJSFile#" crossorigin="anonymous"></script>
			</cfoutput>
		</cfif>
	</body>
</html>
