<div id="sidebarleft" class="col-md-4 col-xl-3 pt-3">
	<ul class="nav flex-column flex-nowrap overflow-hidden pt-2">
			<li class="nav-item">
					<a class="nav-link text-truncate" href="#"><i class="fa fa-home"></i> <span class="d-none d-sm-inline"><strong>Getting Started</strong></span></a>
			</li>
			<li class="nav-item">
				<a class="nav-link collapsed text-truncate" href="#submenu3" data-toggle="collapse" data-target="#submenu3"><i class="fa fa-table"></i> <span class="d-none d-sm-inline">Geocode</span></a>
				<div class="collapse" id="submenu3" aria-expanded="true">
						<ul class="flex-column pl-2 nav">
								<li class="nav-item"><a class="nav-link py-0" href="/demo/ForwardGeocode.cfm"><span>Forward</span></a></li>
								<li class="nav-item"><a class="nav-link py-0" href="/demo/ReverseGeocode.cfm"><span>Reverse</span></a></li>
								<li class="nav-item"><a class="nav-link py-0" href="/demo/BatchGeocode.cfm"><span>Batch</span></a></li>

						</ul>
				</div>
			</li>
	</ul>
</div>
