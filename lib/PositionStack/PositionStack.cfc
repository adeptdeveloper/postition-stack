
<cfcomponent displayname="PositionStack">
	<cfset This.Namespace = 'adept.positionstack'>
	<cfset This.AccountType = 'Basic'>
	<cfset This.Protocol = 'http'>
	<cfset This.BaseEndpoint = '#This.Protocol#://api.positionstack.com/v1'>

	<cfset This.Credentials = StructNew()>
	<cfset This.Credentials.AccessKey = ''>

	<cfset This.Version = StructNew()>
	<cfset This.Version.Major = 1>
	<cfset This.Version.Minor = 0>
	<cfset This.Version.Patch = 0>

	<cffunction name="SetAccessKey" access="public" output="false" returntype="void">
		<cfargument name="AccessKey" type="string" required="true">

		<cfset This.Credentials.AccessKey = Trim(Arguments.AccessKey)>
	</cffunction>

	<cffunction name="Dump" access="public" output="false" returntype="struct">
		<cfreturn This>
	</cffunction>

	<cffunction name="Geocode" access="public" output="false" returntype="struct">
		<cfargument name="Address" type="string" required="true">
		<cfargument name="Country" type="string" required="false" default="" hint="Filter geocoding results by one or multiple comma-separated 2-letter (e.g. AU) or 3-letter country codes (e.g. AUS). Example: country=AU,CA to filter by Australia and Canada.">
		<cfargument name="Region" type="string" required="false" default="" hint="Filter geocoding results by specifying a region. This could be a neighbourhood, district, city, county, state or administrative area. Example: region=Berlin to filter by locations in Berlin.">
		<cfargument name="Language" type="string" required="false" default="en" hint="2 or 3 digit code representing what language to return the response in.">
		<cfargument name="Country_Module" type="boolean" required="false" default="0">
		<cfargument name="Sun_Module" type="boolean" required="false" default="0">
		<cfargument name="Timezone_Module" type="boolean" required="false" default="0">
		<cfargument name="Bbox_Module" type="boolean" required="false" default="0">
		<cfargument name="Limit" type="numeric" required="false" default="10" hint="Number from 1 to 80 indicating how many results you want to see.">
		<cfargument name="Fields" type="string" required="false" default="">
		<cfargument name="Output" type="string" required="false" default="json" hint="Currently only support json. XML and geojson are the other two options.">
		<cfargument name="Callback" type="string" required="false" default="">

		<cfset var EndPoint = This.BaseEndpoint & '/forward?access_key=#This.Credentials.AccessKey#'>
		<cfset var stResponse = StructNew()>

		<cfset stResponse.Error = 0>
		<cfset stResponse.Response = StructNew()>

		<cfif NOT Len(This.Credentials.AccessKey)>
			<cfthrow type="#This.Namespace#" errorcode="MissingAccessKey" message="AccessKey is missing" detail="You must initialize the CFC using the SetAccessKey method to set your PositionTrack Access Key">
		</cfif>

		<cfif NOT Len(Arguments.Address)>
			<cfthrow type="#This.Namespace#" errorcode="NoAddress" message="Address is missing" detail="You must pass an address string to the Geocode method.  Example: 1600 Pennsylvania Ave NW, Washington DC">
		</cfif>

		<!--- Base Endpoint --->
		<cfset EndPoint = EndPoint & '&query=#Arguments.Address#'>

		<cfif Len(Arguments.Country)>
			<cfset EndPoint = EndPoint & '&country=#Arguments.Country#'>
		</cfif>

		<cfif Len(Arguments.Region)>
			<cfset EndPoint = EndPoint & '&region=#Arguments.Region#'>
		</cfif>

		<!--- These features are for paid accounts --->
		<cfif This.AccountType IS NOT 'Free'>
			<cfif Len(Arguments.Language)>
				<cfset EndPoint = EndPoint & '&language=#Arguments.Language#'>
			</cfif>
		</cfif>

		<cfif Len(Arguments.Country_Module) AND Arguments.Country_Module IS 1>
			<cfset EndPoint = EndPoint & '&country_module=1'>
		</cfif>

		<cfif Len(Arguments.Sun_Module) AND Arguments.Sun_Module IS 1>
			<cfset EndPoint = EndPoint & '&sun_module=1'>
		</cfif>

		<cfif Len(Arguments.Timezone_Module) AND Arguments.Timezone_Module IS 1>
			<cfset EndPoint = EndPoint & '&timezone_module=1'>
		</cfif>

		<cfif Len(Arguments.Bbox_Module) AND Arguments.Bbox_Module IS 1>
			<cfset EndPoint = EndPoint & '&bbox_module=1'>
		</cfif>

		<cfif Len(Arguments.Limit) AND IsNumeric(Arguments.Limit) AND Arguments.Limit GT 0 AND Arguments.Limit LTE 80>
			<cfset EndPoint = EndPoint & '&limit=#Arguments.Limit#'>
		</cfif>

		<cfif Len(Arguments.Fields)>
			<cfset EndPoint = EndPoint & '&fields=#Arguments.Fields#'>
		</cfif>

		<cfif Len(Arguments.Output)>
			<cfset EndPoint = EndPoint & '&output=#Arguments.Output#'>
		</cfif>

		<cfif Len(Arguments.Callback)>
			<cfset EndPoint = EndPoint & '&callback=#Arguments.Callback#'>
		</cfif>

		<cflog file="#This.Namespace#" type="information" text="Geocode:#EndPoint#">

		<!--- Call API --->
		<cftry>
			<cfhttp url="#EndPoint#" method="GET">
				<cfhttpparam type="header" name="Content-Type" value="application/json; charset=utf-8">
			</cfhttp>

			<cfset stResponse.Response = DeserializeJson(cfhttp.FileContent)>

			<cfif StructKeyExists(stResponse.Response, 'Error')>
				<cfset stResponse.Error = 1>
			</cfif>

			<cfcatch type="any">
				<cfset stResponse.Error = 1>
				<cflog file="#This.Namespace#" type="error" text="#cfhttp.FileContent#">
				<cflog file="#This.Namespace#" type="error" text="#SerializeJson(cfhttp)#">
			</cfcatch>
		</cftry>

		<cfreturn stResponse>
	</cffunction>

	<cffunction name="ReverseGeocode" access="public" output="false" returntype="struct">
		<cfargument name="Latitude" type="numeric" required="true">
		<cfargument name="Longitude" type="numeric" required="true">
		<cfargument name="Country" type="string" required="false" default="" hint="Filter geocoding results by one or multiple comma-separated 2-letter (e.g. AU) or 3-letter country codes (e.g. AUS). Example: country=AU,CA to filter by Australia and Canada.">
		<cfargument name="Region" type="string" required="false" default="" hint="Filter geocoding results by specifying a region. This could be a neighbourhood, district, city, county, state or administrative area. Example: region=Berlin to filter by locations in Berlin.">
		<cfargument name="Language" type="string" required="false" default="en" hint="2 or 3 digit code representing what language to return the response in.">
		<cfargument name="Country_Module" type="boolean" required="false" default="0">
		<cfargument name="Sun_Module" type="boolean" required="false" default="0">
		<cfargument name="Timezone_Module" type="boolean" required="false" default="0">
		<cfargument name="Bbox_Module" type="boolean" required="false" default="0">
		<cfargument name="Limit" type="numeric" required="false" default="10" hint="Number from 1 to 80 indicating how many results you want to see.">
		<cfargument name="Fields" type="string" required="false" default="">
		<cfargument name="Output" type="string" required="false" default="json" hint="Currently only support json. XML and geojson are the other two options.">
		<cfargument name="Callback" type="string" required="false" default="">

		<cfset var EndPoint = This.BaseEndpoint & '/reverse?access_key=#This.Credentials.AccessKey#'>
		<cfset var stResponse = StructNew()>

		<cfset stResponse.Error = 0>
		<cfset stResponse.Response = StructNew()>

		<cfif NOT Len(This.Credentials.AccessKey)>
			<cfthrow type="#This.Namespace#" errorcode="MissingAccessKey" message="AccessKey is missing" detail="You must initialize the CFC using the SetAccessKey method to set your PositionTrack Access Key">
		</cfif>

		<cfif NOT Len(Arguments.Latitude)>
			<cfthrow type="#This.Namespace#" errorcode="NoLatitude" message="Latitude is missing" detail="You must pass a numeric Latitude value.">
		</cfif>

		<cfif NOT Len(Arguments.Longitude)>
			<cfthrow type="#This.Namespace#" errorcode="NoLongitude" message="Longitude is missing" detail="You must pass a numeric Longitude value.">
		</cfif>

		<!--- Base Endpoint --->
		<cfset EndPoint = EndPoint & '&query=#Arguments.Latitude#,#Arguments.Longitude#'>

		<cfif Len(Arguments.Country)>
			<cfset EndPoint = EndPoint & '&country=#Arguments.Country#'>
		</cfif>

		<cfif Len(Arguments.Region)>
			<cfset EndPoint = EndPoint & '&region=#Arguments.Region#'>
		</cfif>

		<!--- These features are for paid accounts --->
		<cfif This.AccountType IS NOT 'Free'>
			<cfif Len(Arguments.Language)>
				<cfset EndPoint = EndPoint & '&language=#Arguments.Language#'>
			</cfif>
		</cfif>

		<cfif Len(Arguments.Country_Module) AND Arguments.Country_Module IS 1>
			<cfset EndPoint = EndPoint & '&country_module=1'>
		</cfif>

		<cfif Len(Arguments.Sun_Module) AND Arguments.Sun_Module IS 1>
			<cfset EndPoint = EndPoint & '&sun_module=1'>
		</cfif>

		<cfif Len(Arguments.Timezone_Module) AND Arguments.Timezone_Module IS 1>
			<cfset EndPoint = EndPoint & '&timezone_module=1'>
		</cfif>

		<cfif Len(Arguments.Bbox_Module) AND Arguments.Bbox_Module IS 1>
			<cfset EndPoint = EndPoint & '&bbox_module=1'>
		</cfif>

		<cfif Len(Arguments.Limit) AND IsNumeric(Arguments.Limit) AND Arguments.Limit GT 0 AND Arguments.Limit LTE 80>
			<cfset EndPoint = EndPoint & '&limit=#Arguments.Limit#'>
		</cfif>

		<cfif Len(Arguments.Fields)>
			<cfset EndPoint = EndPoint & '&fields=#Arguments.Fields#'>
		</cfif>

		<cfif Len(Arguments.Output)>
			<cfset EndPoint = EndPoint & '&output=#Arguments.Output#'>
		</cfif>

		<cfif Len(Arguments.Callback)>
			<cfset EndPoint = EndPoint & '&callback=#Arguments.Callback#'>
		</cfif>

		<cflog file="#This.Namespace#" type="information" text="Geocode:#EndPoint#">

		<!--- Call API --->
		<cftry>
			<cfhttp url="#EndPoint#" method="GET">
				<cfhttpparam type="header" name="Content-Type" value="application/json; charset=utf-8">
			</cfhttp>

			<cfset stResponse.Response = DeserializeJson(cfhttp.FileContent)>

			<cfif StructKeyExists(stResponse.Response, 'Error')>
				<cfset stResponse.Error = 1>
			</cfif>

			<cfcatch type="any">
				<cfset stResponse.Error = 1>
				<cflog file="#This.Namespace#" type="error" text="#cfhttp.FileContent#">
				<cflog file="#This.Namespace#" type="error" text="#SerializeJson(cfhttp)#">
			</cfcatch>
		</cftry>

		<cfreturn stResponse>
	</cffunction>

</cfcomponent>
