
<cfcomponent>
	<cffunction name="OnApplicationStart">
		<cfset This.Name = 'AdeptPositionStack'>
		<cfset This.ApplicationName = This.Name>
		<cfset This.RootDir = GetDirectoryFromPath(GetCurrentTemplatePath())>
		<cfset This.ApplicationTimeout = CreateTimeSpan(0,2,0,0)>
		<cfset This.SessionManagement = true>
		<cfset This.SessionTimeout = CreateTimeSpan(0,0,20,0)>
		<cfset This.Mappings[ "/cflib" ] = "#This.RootDir#lib\" />

		<cfset SettingsFile =  FileRead(ExpandPath('/demo/settings.json'))>

		<cfset Application.Settings = DeserializeJson(SettingsFile)>
	</cffunction>

	<cffunction name="OnApplicationEnd">
		<cfset StructClear(Session)>
	</cffunction>

	<cffunction name="OnRequestStart">
		<cfset RequestAttributes()>

		<cfif IsDefined('Request.Attributes.Reset') AND Request.Attributes.Reset IS 1>
			<cfset OnApplicationEnd()>
			<cfset OnApplicationStart()>
		</cfif>
	</cffunction>

	<cffunction name="OnRequest">
		<cfargument name="targetPage" type="String" required=true/>

		<cfif CGI.SCRIPT_NAME DOES NOT CONTAIN '/_'>
			<cfinclude template="/demo/_inc/header.cfm">
		</cfif>

		<cfinclude template="#Arguments.targetPage#">

		<cfif CGI.SCRIPT_NAME DOES NOT CONTAIN '/_'>
			<cfinclude template="/demo/_inc/footer.cfm">
		</cfif>
	</cffunction>

	<cffunction name="OnRequestEnd">
	</cffunction>

	<cffunction name="RequestAttributes" access="private" output="false" returntype="void">
		<cfset var itm = ''>

		<cfset Request.Attributes = StructNew()>
		<cfset Request.Attributes.Encoded = StructNew()>
		<cfset Request.Attributes.Encoded.Html = StructNew()>
		<cfset Request.Attributes.Encoded.Css = StructNew()>
		<cfset Request.Attributes.Encoded.Js = StructNew()>
		<cfset Request.Attributes.Encoded.Url = StructNew()>
		<cfset Request.Attributes.Encoded.Xml = StructNew()>
		<cfset Request.Attributes.Encoded.HtmlAttribute = StructNew()>

		<cfloop item="itm" collection="#Form#">
			<cfset Request.Attributes[itm] = Trim(Form[itm])>
		</cfloop>

		<cfloop item="itm" collection="#Url#">
			<cfset Request.Attributes[itm] = Trim(Url[itm])>
		</cfloop>

		<!--- Canonicalize all input data to remove any mixed encoded strings --->
		<cfloop collection="#Request.Attributes#" item="itm">
			<cfif itm IS NOT 'Encoded'>
				<cfset Request.Attributes[itm] = Canonicalize(Request.Attributes[itm], true, true)>

				<cfset Request.Attributes.Encoded.Html[itm] = EncodeForHtml(Request.Attributes[itm])>
				<cfset Request.Attributes.Encoded.Css[itm] = EncodeForCss(Request.Attributes[itm])>
				<cfset Request.Attributes.Encoded.Js[itm] = EncodeForJavascript(Request.Attributes[itm])>
				<cfset Request.Attributes.Encoded.Html[itm] = EncodeForUrl(Request.Attributes[itm])>
				<cfset Request.Attributes.Encoded.Html[itm] = EncodeForXml(Request.Attributes[itm])>
				<cfset Request.Attributes.Encoded.Html[itm] = EncodeForHtmlAttribute(Request.Attributes[itm])>
			</cfif>
		</cfloop>
	</cffunction>
</cfcomponent>
